<?php
if(!empty($_COOKIE['taskData'])){                         // Проверяем, существует ли суперглобальный массив COOKIE
    $tasksList = json_decode($_COOKIE['taskData'], true); // Переводим данные из JSON формата в PHP массив и далее работаем с ним
}
?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Домашнее задание №10 Гурца Алексея</title>
    <meta name="description" content="Работа с COOKIES">
    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet"
    integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
</head>

<body>
    <div class="container-fluid">
            <header class="p-3 m-3 text-warning" style="background-color: #4b6477;">
                <div class="text-center">
                    <h1>Домашнее задание №10</h1>
                    <h2>Работа с COOKIES. Создание TODO list с использованием COOKIES</h2>
                </div>
            </header>
    </div>
    <div class="container">
        <div class="row">
            <form action="/addTask.php" method="POST" class="w-50 p-3 mb-3" style="background-color: tan;">
                <div class="mb-3">
                    <textarea class="form-control" name="task_text" placeholder="Введите ваш текст задачи"></textarea>
                </div>
                <button type="submit" class="btn btn-warning">Добавить задачу</button>
            </form>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <form action="delete_cookie.php" method="POST">
                <div class="mb-3">
                    <input type="hidden" name="delcookie">
                    <button class="btn btn-danger">Удалить cookie!</button>
                </div>
            </form>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <table class="table table-secondary table-hover table caption-top">
                <caption>TODO list таблица</caption>
                <thead class="table-warning">
                    <tr style="text-align: center;">
                        <th scope="col">Порядковый номер задачи</th>
                        <th scope="col">Текст задачи</th>
                        <th scope="col">Состояние задачи</th>
                        <th scope="col">Выполнение</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if($tasksList != null): // Если массив не пустой, начинаем вывод информации в таблицу перебором массива ?>  
                        <?php foreach($tasksList as $key => $task): ?>
                            <tr style="text-align: center;">
                                <th scope="row">#<?=(int)$key + 1; // Добавляем единицу для нумерации заданий начиная с "1" ?></th>
                                <td><?=$task['title']; ?></td>
                                <td>
                                    <?php if($task['completed'] == false):
                                            echo 'не выполнена';
                                        else:
                                            echo 'выполнена';
                                    endif; ?>
                                </td>
                                <td>
                                    <form action="/completeTask.php" method ="GET">
                                        <input type="hidden" name="task_number" value="<?=$key; ?>">
                                        <button type="submit" class="btn btn-primary">Выполнить задачу</button>
                                    </form>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </tbody>
            </table>
        </div>
    </div>
    <div class="container-fluid">
        <footer class="d-flex justify-content-center p-1 m-1 text-warning" style="background-color: #4b6477;">
            <p>Гурец Алексей &copy;2021 <a href="mailto:oleksii.gurets@gmail.com" class="text-warning">Все вопросы по
                    почте</a> <a href="/" class="text-warning">Главная</a></p>
        </footer>
    </div>     
</body>

</html>