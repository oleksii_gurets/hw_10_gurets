<?php
if(!empty($_POST['task_text'])){

    $taskArray = []; // Создаем пустой массив для будущих данных из задач
    
    if(!empty($_COOKIE['taskData'])){
        $taskArray = json_decode($_COOKIE['taskData'], true);
    }
    if(!empty($taskArray)){      // Если в массиве есть данные, то добавляем данные следующей новой задачи из формы
        $taskArray[] = [
            'title' => $_POST['task_text'],
            'completed' => false
        ];
    }else{
        $taskArray[] = [        // Если массив пустой, то заполняем его данными первой задачи
            'title' => $_POST['task_text'],
            'completed' => false
        ];
    }
    setcookie("taskData", json_encode($taskArray), time() + 180); //Сохраняем текущий массив задач в cookie на 3 минуты
    header('Location:/index.php');
}else{
    header('Location:/index.php');
}
?>